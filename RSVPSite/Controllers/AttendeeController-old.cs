﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class AttendeeController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /Attendee/

        public ActionResult Index()
        {
            return View(db.Register.ToList());
        }

        //
        // GET: /Attendee/Details/5

        public ActionResult Details(int id = 0)
        {
            Attendee.Models Attendee = db.Register.Find(id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //
        // GET: /Attendee/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Attendee/Create

        [HttpPost]
        public ActionResult Create(Models Attendee)
        {

            if (ModelState.IsValid)
            {
                string userkey = RandomKey(); ;
                  db.Register.Add(Attendee);

                 db.Register.Add(Attendee.UserKey);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(Attendee);
        }

        //
        // GET: /Attendee/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Models.Attendee  = db.Register.Find(id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //
        // POST: /Attendee/Edit/5

        [HttpPost]
        public ActionResult Edit(Models.Attendee Attendee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Attendee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Attendee);
        }

        //
        // GET: /Attendee/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Models Attendee = db.Register.Find(id);
            if (Attendee == null)
            {
                return HttpNotFound();
            }
            return View(Attendee);
        }

        //
        // POST: /Attendee/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Models Attendee = db.Register.Find(id);
            db.Register.Remove(Attendee);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

           public string RandomKey()
           {
            var chars = "ABCDEFGH0123458JKLMNOPQRSTUVWXYZ";
            var random = new Random();

            var UserKey = new string(
                Enumerable.Repeat(chars, 10)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());
            return UserKey;
        }
    }
}