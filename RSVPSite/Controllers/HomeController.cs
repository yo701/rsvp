﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modified the ViewBag.Message in HomeController ---- tada";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "The goal is to create an event, register an Attendee, and view";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact forthcoming";

            return View();
        }
    }
}
