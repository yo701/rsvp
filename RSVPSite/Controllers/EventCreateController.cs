﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    //TODO: SO - Harvest the good bits - move to the EventController
    //public class EventController : Controller
    //{
    //    private RSVPDBContext db = new RSVPDBContext();

    //    //
    //    // GET: /Event/

    //    public ActionResult Index()
    //    {
    //        return View(db.Events.ToList());
    //    }

    //    //
    //    // GET: /Event/Details/5

    //    public ActionResult Details(string events)
    //    {
    //        Event Event = db.Events.Find(events);
    //        if (Event == null)
    //        {
    //            return HttpNotFound();
    //        }
    //        return View(Event);
    //    }

    //    //
    //    // GET: /Event/Create

    //    public ActionResult Create()
    //    {
    //        return View();
    //    }

    //    //
    //    // POST: /Event/Create

    //    [HttpPost]
    //    public ActionResult Create(Event Event)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            db.Events.Add(Event);
    //            db.SaveChanges();
    //            return RedirectToAction("Index");
    //        }

    //        return View(Event);
    //    }

    //    //
    //    // GET: /Event/Edit/5

    //    public ActionResult Edit(string events)
    //    {
    //        Event Event = db.Events.Find(events);
    //        if (Event == null)
    //        {
    //            return HttpNotFound();
    //        }
    //        return View(Event);
    //    }

    //    //
    //    // POST: /Event/Edit/5

    //    [HttpPost]
    //    public ActionResult Edit(Event Event)
    //    {
    //        if (ModelState.IsValid)
    //        {
    //            db.Entry(Event).State = EntityState.Modified;
    //            db.SaveChanges();
    //            return RedirectToAction("Index");
    //        }
    //        return View(Event);
    //    }

    //    //
    //    // GET: /Event/Delete/5

    //    public ActionResult Delete(string events)
    //    {
    //        Event Event = db.Events.Find(events);
    //        if (Event == null)
    //        {
    //            return HttpNotFound();
    //        }
    //        return View(Event);
    //    }

    //    //
    //    // POST: /Event/Delete/5

    //    [HttpPost, ActionName("Delete")]
    //    public ActionResult DeleteConfirmed(string events)
    //    {
    //        Event Event = db.Events.Find(events);
    //        db.Events.Remove(Event);
    //        db.SaveChanges();
    //        return RedirectToAction("Index");
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        db.Dispose();
    //        base.Dispose(disposing);
    //    }
    //}
}