﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Cts.HR.Recruiting.RSVP.Models;
using Cts.HR.Recruiting.RSVP.DAL;
using Cts.HR.Recruiting.RSVP.ViewModels;

namespace Cts.HR.Recruiting.RSVP.Controllers
{
    public class EventController : Controller
    {
        private RSVPDBContext db = new RSVPDBContext();

        //
        // GET: /Event/

        public ActionResult Index()
        {
            var model = db.Events.Select(e => new ViewModels.EventData { Event = e })
                .ToList();
            return View(model);
        }

        //
        // GET: /Event/Details/5

        public ActionResult Details(int id = 0)
        {
            Event Event = db.Events.Find(id);
            if (Event == null)
            {
                return HttpNotFound();
            }
            return View(Event);
        }

        //
        // GET: /Event/Create

        public ActionResult Create()
        {

            var viewModel = new ViewModels.EventData
            {
                EventTypes = db.EventTypes
            };

            return View(viewModel);
        }

        //
        // POST: /Event/Create

        //[HttpPost]
        //public ActionResult Create(Event Event)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Events.Add(Event);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(Event);
        //}

        [HttpPost]
        public ActionResult Create(ViewModels.EventData eventData) //since viewmodels.eventdata is being used in the view, that's what it's passed here as param
        {
            if (ModelState.IsValid) //validation of model is determined by your validations made for that model
            {
                db.Events.Add(eventData.Event);
                db.Questions.Add(eventData.Question);
                db.SaveChanges(); //got an error here. 
                                    //the error message told me that when this method insert a data here, fk_event.typeid has conflict with eventtype.id. 
                                    //prob fixed after changing the dropdownlist to dropdownlistfor in the view.                
                return RedirectToAction("Index");
            }
            return View(eventData.Event);
        }

        //
        // GET: /Event/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Event Event = db.Events.Find(id);
            if (Event == null)
            {
                return HttpNotFound();
            }
            return View(Event);
        }

        //
        // POST: /Event/Edit/5

        [HttpPost]
        public ActionResult Edit(Event Event)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Event).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(Event);
        }

        //
        // GET: /Event/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Event Event = db.Events.Find(id);
            if (Event == null)
            {
                return HttpNotFound();
            }
            return View(Event);
        }

        //
        // POST: /Event/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Event Event = db.Events.Find(id);
            db.Events.Remove(Event);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}

//*******************************************************************************
//Code from the EventCreateController using --> public ActionResult Whatever(string events)
//It uses a 'string' instead of an 'int' to pull the Event up for Editing, Deleting, Viewing
//*******************************************************************************
// Example:
//       public ActionResult Details(string events)    <---using a 'string'
//    {
//        Event Event = db.Events.Find(events);
//        if (Event == null)
//        {
//            return HttpNotFound();
//        }
//        return View(Event);
//    }
//
//*******************************************************************************
//It's helpful because in the "View"  --> Event --> Index.cshtml 
//You can do the same thing with an 'int'  -- it is going to be  a preference 
//and ease of access/use (like is it easier to get to the 'int' or easier to get to the 'string')
//we can use a @foreach to create a link inde<ul>
//    @foreach (var events in Model)
//    { 
//    <li>@Html.ActionLink(events.EventName, "Details", new {events = events.EventName})</li>
//    }
//</ul>
//
//*******************************************************************************x automatically
//Example: 