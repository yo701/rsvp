﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Cts.HR.Recruiting.RSVP.Models;

namespace Cts.HR.Recruiting.RSVP.DAL
{
    public class EventInitializer : DropCreateDatabaseIfModelChanges<RSVPDBContext>
    {
        protected override void Seed(RSVPDBContext context)
        {
            var Events = new List<Event>
                {
                    new Event {Id = 1, EventDate = DateTime.Today, Location = "USA", EventTypeId=1}
                };
            Events.ForEach(a => context.Events.Add(a));
            context.SaveChanges();

            var EventTypes = new List<EventType>
                {
                    new EventType {Id = 1, Name = "Campus"},
                    new EventType {Id = 2, Name = "Corporate"}
                };

            EventTypes.ForEach(a => context.EventTypes.Add(a));
            context.SaveChanges();
        }
    }
}