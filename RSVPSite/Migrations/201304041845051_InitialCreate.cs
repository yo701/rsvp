namespace Cts.HR.Recruiting.RSVP.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        EventDate = c.DateTime(nullable: false),
                        EventTypeID = c.Int(),
                        Location = c.String(nullable: false),
                        Address1 = c.String(),
                        Address2 = c.String(),
                        City = c.String(),
                        State = c.String(),
                        Zip = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventTypes", t => t.EventTypeID)
                .Index(t => t.EventTypeID);
            
            CreateTable(
                "dbo.CustomQuestions",
                c => new
                    {
                        CustomQuestionID = c.Int(nullable: false, identity: true),
                        EventID = c.Int(nullable: false),
                        Question = c.String(),
                    })
                .PrimaryKey(t => t.CustomQuestionID)
                .ForeignKey("dbo.Events", t => t.EventID, cascadeDelete: true)
                .Index(t => t.EventID);
            
            CreateTable(
                "dbo.EventTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Attendees",
                c => new
                    {
                        EventAttendeeId = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        Company = c.String(),
                        School = c.String(),
                        Address = c.String(),
                        WebURL = c.String(),
                        AttendeeGuid = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.EventAttendeeId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserID);
            
            CreateTable(
                "dbo.AttendeeEvents",
                c => new
                    {
                        Attendee_EventAttendeeId = c.Int(nullable: false),
                        Event_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Attendee_EventAttendeeId, t.Event_Id })
                .ForeignKey("dbo.Attendees", t => t.Attendee_EventAttendeeId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.Event_Id, cascadeDelete: true)
                .Index(t => t.Attendee_EventAttendeeId)
                .Index(t => t.Event_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.AttendeeEvents", new[] { "Event_Id" });
            DropIndex("dbo.AttendeeEvents", new[] { "Attendee_EventAttendeeId" });
            DropIndex("dbo.CustomQuestions", new[] { "EventID" });
            DropIndex("dbo.Events", new[] { "EventTypeID" });
            DropForeignKey("dbo.AttendeeEvents", "Event_Id", "dbo.Events");
            DropForeignKey("dbo.AttendeeEvents", "Attendee_EventAttendeeId", "dbo.Attendees");
            DropForeignKey("dbo.CustomQuestions", "EventID", "dbo.Events");
            DropForeignKey("dbo.Events", "EventTypeID", "dbo.EventTypes");
            DropTable("dbo.AttendeeEvents");
            DropTable("dbo.Users");
            DropTable("dbo.Attendees");
            DropTable("dbo.EventTypes");
            DropTable("dbo.CustomQuestions");
            DropTable("dbo.Events");
        }
    }
}
