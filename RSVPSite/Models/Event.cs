﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class Event
    {
        public Event()
        {
            this.CustomQuestions = new List<CustomQuestion>();
            this.Attendees = new List<Attendee>();
        }

        [Required]
        public string Name { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Display(Name="Date")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime EventDate { get; set; }

        [Required]
        public int EventTypeId { get; set; }

        [Required]
        public string Location { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }

        public virtual ICollection<CustomQuestion> CustomQuestions { get; set; }
        public virtual EventType EventType { get; set; }
        public virtual ICollection<Attendee> Attendees { get; set; }
    }
}