﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class RSVPDBContext : DbContext
    {
        public RSVPDBContext() : base("DefaultConnection")
        {

        }

        public DbSet<Event> Events { get; set; }
        public DbSet<Attendee> Register { get; set; }
        public DbSet<User> Login { get; set; }

        public DbSet<CustomQuestion> Questions { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
    }
}