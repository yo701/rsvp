﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.ComponentModel.DataAnnotations;

    namespace Cts.HR.Recruiting.RSVP.Models
    {
            
   

        public class Attendee
        {
            [Key]
            public int EventAttendeeId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
            public string Company { get; set; }
            public string School { get; set; }
            public string Address { get; set; }
            public string WebURL { get; set; }
            public string UserKey { get; set; }
          
            public virtual ICollection<Event> Events { get; set; }

        }

    }
