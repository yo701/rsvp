﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Cts.HR.Recruiting.RSVP.Models
{
    public class CustomQuestion
    {
        public int CustomQuestionID { get; set; }
        public int EventID { get; set; }
        public string Question { get; set; }

        public virtual Event Event { get; set; }
    }
}